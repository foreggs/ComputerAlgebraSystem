package com.d0minikt.lib

import java.math.BigDecimal
import kotlin.math.absoluteValue
import kotlin.math.pow

class Fraction: Comparable<Fraction> {
    companion object {
        fun simplify(numerator: Long, denominator: Long): Pair<Long, Long> {
            var n = numerator
            var d = denominator

            val gcd = gcd(numerator, denominator)
            if (gcd > 1L) {
                n = numerator / gcd
                d = denominator / gcd
            }
            if (denominator < 0L) {
                n = -numerator
                d = -denominator
            }
            return Pair(n, d)
        }

        fun gcd(a: Long, b: Long): Long =
            if (b == 0L)
                a.absoluteValue
            else
                gcd(b.absoluteValue, a.absoluteValue.mod(b.absoluteValue))

        fun fromBigDecimal(n: BigDecimal): Fraction {
            if (n == BigDecimal.ZERO)
                return Fraction(0L)
            val ds = n.toString().trimEnd('.')
            val index = ds.indexOf('.')
            if (index == -1) return Fraction(ds.toLong())
            var num = ds.replace(".", "").toLong()
            var den = 1L
            for (n in 1..(ds.length - index - 1)) den *= 10L
            while (num % 2L == 0L && den % 2L == 0L) {
                num /= 2L
                den /= 2L
            }
            while (num % 5L == 0L && den % 5L == 0L) {
                num /= 5L
                den /= 5L
            }
            return Fraction(num.toLong(), den.toLong())
        }
    }

    val numerator: Long
    val denominator: Long


    constructor(numerator: Long, denominator: Long = 1L) {
        if (denominator == 0L) throw Exception("Cannot Divide by 0")

        val (n, d) = simplify(numerator, denominator)
        this.numerator = n
        this.denominator = d
    }

    operator fun times(other: Fraction): Fraction = Fraction(
            numerator * other.numerator,
            denominator * other.denominator
    )
    operator fun div(other: Fraction): Fraction = times(other.reciprocal())
    operator fun plus(other: Fraction): Fraction = Fraction(
            numerator * other.denominator + other.numerator * denominator,
            denominator * other.denominator
    )
    operator fun minus(other: Fraction): Fraction = plus(other.negate())
    operator fun unaryMinus(): Fraction = Fraction(
            -numerator,
            denominator
    )
    // kotlin (just like java) does not support the ^ operator
    fun pow(other: Int): Fraction {
        val n = numerator.toDouble().pow(other).toLong()
        val d = denominator.toDouble().pow(other).toLong()
        return Fraction(n, d)
    }
    fun reciprocal(): Fraction = Fraction(denominator, numerator)
    fun negate(): Fraction = Fraction(-numerator, denominator)

    fun toBigDecimal(): BigDecimal = numerator.toBigDecimal().divide(denominator.toBigDecimal())

    override fun compareTo(other: Fraction): Int {
        val n1: Long = numerator * other.denominator
        val n2: Long = other.numerator * denominator
        return n1.compareTo(n2)
    }

    override fun equals(other: Any?): Boolean {
        if (other is Fraction) return compareTo(other) == 0

        return false
    }

    override fun toString(): String {
        if (denominator == 1L)
            return "$numerator"
        return "$numerator/$denominator"
    }
}