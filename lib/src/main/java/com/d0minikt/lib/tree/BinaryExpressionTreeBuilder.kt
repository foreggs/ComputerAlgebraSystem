package com.d0minikt.lib.tree

import com.d0minikt.lib.operatorMap
import com.d0minikt.lib.token.*
import java.util.Stack

class BinaryExpressionTreeBuilder {
    lateinit var rpn: Stack<Token>

    fun createFromInfix(tokens: List<Token>): BinaryExpressionTree {
        rpn = infixToRPN(tokens)
        return rpnToBinaryExpressionTree()
    }

    fun createFromPostfix(rpn: Stack<Token>): BinaryExpressionTree {
        this.rpn = rpn
        return rpnToBinaryExpressionTree()
    }

    private fun rpnToBinaryExpressionTree(parentP: BinaryExpressionTree? = null): BinaryExpressionTree {
        val node = BinaryExpressionTree(rpn.pop())

        while (!node.isFull()) {
            if (node.right == null)
                node.right = rpnToBinaryExpressionTree()
            else
                node.left = rpnToBinaryExpressionTree()
        }
        return node
    }

    fun infixToRPN(tokens: List<Token>): Stack<Token> {
        val tokenStack: Stack<Token> = Stack()
        val outputStack: Stack<Token> = Stack()
        val operatorStack: Stack<Token> = Stack()

        tokenStack.addAll(tokens.reversed())

        while (tokenStack.isNotEmpty()) {
            val token: Token = tokenStack.pop()
            if (token is NumberToken || token is VariableToken)
                outputStack.push(token)
            else if (token is OperatorToken) {
                while(operatorStack.isNotEmpty()
                        && operatorStack.peek() is OperatorToken
                        && (operatorMap[token.symbol]!!.precedence < operatorMap[(operatorStack.peek() as OperatorToken).symbol]!!.precedence||(!operatorMap[(operatorStack.peek() as OperatorToken).symbol]!!.isRightAssociative && operatorMap[token.symbol]!!.precedence == operatorMap[(operatorStack.peek() as OperatorToken).symbol]!!.precedence))
                ) {
                    outputStack.push(operatorStack.pop())
                }
                operatorStack.push(token)
            } else if (token is BracketToken) {
                if (token.type == BracketType.Left) {
                    operatorStack.push(token)
                } else {
                    if (operatorStack.isNotEmpty()) {
                        while (operatorStack.isNotEmpty() && operatorStack.peek().toString() != "(") {
                            outputStack.push(operatorStack.pop())
                        }
                        if (operatorStack.peek() is BracketToken && (operatorStack.peek() as BracketToken).type != BracketType.Left)
                            throw Exception("Mismatching brackets")
                        if (operatorStack.isNotEmpty())
                            operatorStack.pop()
                    }
                }
            }
        }

        while (operatorStack.isNotEmpty()) {
            if (operatorStack.peek() is BracketToken)
                throw Exception("Math Error: Mismatching brackets!")
            outputStack.push(operatorStack.pop())
        }

        return outputStack
    }
}