package com.d0minikt.lib.tree

import com.d0minikt.lib.Fraction
import com.d0minikt.lib.token.*

class BinaryExpressionTree(var token: Token) {
    var left: BinaryExpressionTree? = null
    var right: BinaryExpressionTree? = null

    fun isFull(): Boolean =
            if (token is OperatorToken)
                left is BinaryExpressionTree && right is BinaryExpressionTree
            else
                true

    fun print(tabs: Int = 0) {
        print("  ".repeat(tabs))
        println("$token")
        left?.print(tabs+1)
        right?.print(tabs+1)
    }

    fun simplify() {
        left?.simplify()
        right?.simplify()

        val token: Token = this.token
        val l = left?.token
        val r = right?.token

        if (token is OperatorToken) {
            if (token.symbol == '^' && r is NumberToken && r.value == Fraction(1)) {
                this.token = l!!
                emptyChildren()
            } else if (token.symbol == '^' && r is NumberToken && r.value == Fraction(0)) {
                this.token = NumberToken(Fraction(1))
                emptyChildren()
            } else if (token.symbol == '^' && l is NumberToken && l.value == Fraction(1)) {
                this.token = l
                emptyChildren()
            } else if (token.symbol == '+' && l is NumberToken && l.value == Fraction(0)) {
                this.token = r!!
                emptyChildren()
            } else if (token.symbol == '+' && r is NumberToken && r.value == Fraction(0)) {
                this.token = l!!
                emptyChildren()
            } else if (token.symbol == '-' && r is NumberToken && r.value == Fraction(0)) {
                this.token = l!!
                emptyChildren()
            } else if (token.symbol == '-' && l is NumberToken && l.value == Fraction(0) && r is ValueToken) {
                this.token = r.negate()
                emptyChildren()
            } else if (token.symbol == '*' && l is NumberToken && l.value == Fraction(1)) {
                this.token = r!!
                emptyChildren()
            } else if ((token.symbol == '*' || token.symbol == '/') && r is NumberToken && r.value == Fraction(1)) {
                this.token = l!!
                emptyChildren()
            } else if (token.symbol == '*' && (r is NumberToken && r.value == Fraction(0) || l is NumberToken && l.value == Fraction(0))) {
                this.token = NumberToken(Fraction(0))
                emptyChildren()
            } else if (token.symbol == '^' && l is NumberToken && r is NumberToken) {
                if (r.value.denominator == 1L) {
                    this.token = NumberToken(l.value.pow(r.value.numerator.toInt()))
                    emptyChildren()
                }else {
                    if (r.value.numerator < 0L)
                        this.left?.token = NumberToken(l.value.reciprocal())
                    else
                        this.left?.token = NumberToken(l.value.pow(r.value.numerator.toInt()))
                    this.right?.token = NumberToken(Fraction(1, r.value.denominator))
                }
            } else if (token.symbol == '+' && l is NumberToken && r is NumberToken) {
                this.token = NumberToken(l.value + r.value)
                emptyChildren()
            } else if (token.symbol == '-' && l is NumberToken && r is NumberToken) {
                this.token = NumberToken(l.value - r.value)
                emptyChildren()
            } else if (token.symbol == '*' && l is NumberToken && r is NumberToken) {
                this.token = NumberToken(l.value * r.value)
                emptyChildren()
            } else if (token.symbol == '/' && l is NumberToken && r is NumberToken) {
                this.token = NumberToken(l.value / r.value)
                emptyChildren()
            }
        }
    }

    private fun emptyChildren() {
        this.left = null
        this.right = null
    }
}