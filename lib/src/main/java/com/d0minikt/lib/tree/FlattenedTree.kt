package com.d0minikt.lib.tree

import com.d0minikt.lib.token.*

class FlattenedTree {
    companion object {
        fun flatten(root: BinaryExpressionTree): FlattenedTree {
            val children = mutableListOf<FlattenedTree>()
            if (root.token.toString() == "+" && root.left?.token.toString() == "+") {
                children.addAll(flatten(root.left!!).children)
            } else {
                children.add(FlattenedTree(root.left!!))
            }
            if (root.token.toString() == "+" && root.right?.token.toString() == "+") {
                children.addAll(flatten(root.right!!).children)
            } else {
                children.add(FlattenedTree(root.right!!))
            }
            return FlattenedTree(root.token, children)
        }
    }


    var token: Token
    var children: MutableList<FlattenedTree> = mutableListOf()

    constructor(token: Token, children: MutableList<FlattenedTree> = mutableListOf()) {
        this.token = token
        this.children = children
    }

    constructor(bet: BinaryExpressionTree) {
        token = bet.token
        if (bet.left != null) children.add(FlattenedTree(bet.left!!))
        if (bet.right != null) children.add(FlattenedTree(bet.right!!))
    }

    fun print(tabs: Int = 0) {
        print("  ".repeat(tabs))
        println("$token")
        for(item in children) {
            item.print(tabs + 1)
        }
    }
}