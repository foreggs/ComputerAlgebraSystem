package com.d0minikt.lib.token

abstract class ValueToken: Token {
    abstract fun negate(): ValueToken
}