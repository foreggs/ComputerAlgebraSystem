package com.d0minikt.lib.token

interface Token {
    override fun toString(): String
}