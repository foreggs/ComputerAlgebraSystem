package com.d0minikt.lib.token

enum class BracketType {
    Left,
    Right
}

class BracketToken(val type: BracketType): Token {
    override fun toString(): String {
        return if(type == BracketType.Left) "(" else ")"
    }
}