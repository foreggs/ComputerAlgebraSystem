package com.d0minikt.lib.token

class VariableToken(val symbol: Char, val negative: Boolean = false): ValueToken() {

    override fun negate(): VariableToken = VariableToken(symbol, !negative)

    override fun toString(): String = "${if (negative) "-" else ""}$symbol"
}