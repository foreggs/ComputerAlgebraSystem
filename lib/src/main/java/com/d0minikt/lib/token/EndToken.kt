package com.d0minikt.lib.token

class EndToken: Token {
    override fun toString(): String = "[EndToken]"
}