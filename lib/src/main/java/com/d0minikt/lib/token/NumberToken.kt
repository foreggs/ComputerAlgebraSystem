package com.d0minikt.lib.token

import com.d0minikt.lib.Fraction
import java.math.BigDecimal

class NumberToken(val value: Fraction): ValueToken() {
    constructor(decimal: BigDecimal): this(Fraction.fromBigDecimal(decimal))

    override fun negate(): NumberToken = NumberToken(value.negate())

    override fun toString(): String {
        try {
            return value.toBigDecimal().toString()
        } catch (err: Exception) {
            return value.toString()
        }
    }
}