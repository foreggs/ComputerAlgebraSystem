package com.d0minikt.lib.token

class OperatorToken(val symbol: Char): Token {
    override fun toString(): String = "$symbol"
}