package com.d0minikt.lib.lexer

enum class State {
    Initial,
    NoNextState,

    WholeNumber,
    DecimalNumber,
    IncompleteDecimalNumber,
    Variable,

    Equals,
    Add,
    Subtract,
    Multiply,
    Divide,
    Power,
}