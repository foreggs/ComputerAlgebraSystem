package com.d0minikt.lib.lexer

data class FSMResult(val state: State, val accepted: Boolean, val length: Int)

class FSM(
        private val acceptingStates: Array<State>,
        private val nextState: (current: State, character: Char) -> State
) {
    fun run(input: String): FSMResult {
        var currentState = State.Initial

        for (i in input.indices) {
            val char = input[i]
            val nextState: State = nextState(currentState, char)

            if (nextState == State.NoNextState) {
                return FSMResult(currentState, acceptingStates.contains(currentState), i)
            }
            currentState = nextState
        }

        return FSMResult(currentState, acceptingStates.contains(currentState), input.length)
    }
}
