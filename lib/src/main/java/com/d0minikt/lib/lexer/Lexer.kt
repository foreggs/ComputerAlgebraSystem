package com.d0minikt.lib.lexer

import com.d0minikt.lib.operatorMap
import com.d0minikt.lib.token.*

class Lexer(private val input: String) {
    private var position: Int = 0
    private var tokens: MutableList<Token> = mutableListOf()
    private var polarity: Char = '+'

    fun tokenize(): List<Token> {
        var next: Token = nextToken()

        while(next !is EndToken) {
            tokens.add(next)
            next = nextToken()

            val last = tokens.last()
            if (last is NumberToken && next is VariableToken || last is VariableToken && next is VariableToken || (last is BracketToken && last.type == BracketType.Right || last is NumberToken || last is VariableToken) && next is BracketToken && next.type == BracketType.Left)
                tokens.add(OperatorToken('*'))
            else if (last is VariableToken && next is NumberToken)
                tokens.add(OperatorToken('^'))
        }

        return tokens
    }

    private fun nextToken(): Token {
        ignoreWhitespace()

        if (position >= this.input.length)
            return EndToken()

        if (tokens.count() > 0 && (tokens.last() is OperatorToken || tokens.last() is BracketToken) || tokens.count() == 0)
            skipOperatorsBeforeFirstNumber()

        val character: Char = input[position]

        if(character.isDigit() || character == '.' || character.isLetter() || operatorMap.containsKey(character)){
            val result: FSMResult = AlgebraFSM.run(input.substring(position))
            var output: String = input.substring(position, position + result.length)
            var negative: Boolean = false

            if (polarity == '-') {
                if (result.state == State.DecimalNumber || result.state == State.WholeNumber) {
                    output = "-$output"
                    polarity = '+'
                } else if (result.state == State.Variable) {
                    negative = true
                }
            }

            if (!result.accepted) throw Exception("Invalid token")

            position += result.length

            return when(result.state) {
                State.DecimalNumber -> NumberToken(output.toBigDecimal())
                State.WholeNumber -> NumberToken(output.toBigDecimal())
                State.Variable -> VariableToken(output[0], negative)

                State.Equals -> OperatorToken('=')
                State.Add -> OperatorToken('+')
                State.Subtract -> OperatorToken('-')
                State.Multiply -> OperatorToken('*')
                State.Divide -> OperatorToken('/')
                State.Power -> OperatorToken('^')

                else -> {
                    throw Exception("Unhandled state ${result.state}")
                }
            }
        }
        if (character == '(') {
            position++
            return BracketToken(BracketType.Left)
        }
        if (character == ')') {
            position++
            return BracketToken(BracketType.Right)
        }

        throw Exception("Invalid character")
    }

    private fun ignoreWhitespace() {
        while (position < input.length && input[position].isWhitespace()) {
            position++
        }
    }
    private fun skipOperatorsBeforeFirstNumber() {
        while(position < input.length && operatorMap.containsKey(input[position]) && input[position] == '-' || input[position] == '+') {
            polarity = simplifyOperators(input[position], polarity)
            position++
        }
    }

    private fun simplifyOperators(c1: Char, c2: Char): Char {
        when (c1) {
            '+' -> {
                if (c2 == '-') return '-'
                if (c2 == '+') return '+'
            }
            '-' -> {
                if (c2 == '-') return '+'
                if (c2 == '+') return '-'
            }
        }
        // could not combine
        throw Exception("Cannot combine $c1 amd $c2")
    }
}