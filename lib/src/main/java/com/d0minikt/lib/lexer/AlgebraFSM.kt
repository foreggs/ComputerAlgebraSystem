package com.d0minikt.lib.lexer


val AlgebraFSM = FSM(
        arrayOf(State.WholeNumber, State.DecimalNumber, State.Variable, State.Equals, State.Add, State.Subtract, State.Multiply, State.Divide, State.Power),
        nextState = fun(currentState: State, char: Char): State {
            when (currentState) {
                State.Initial -> {
                    if (char.isDigit()) return State.WholeNumber
                    if (char.isLetter()) return State.Variable
                    when(char) {
                        '=' -> return State.Equals
                        '+' -> return State.Add
                        '-' -> return State.Subtract
                        '*' -> return State.Multiply
                        '/' -> return State.Divide
                        '^' -> return State.Power
                    }
                }
                State.WholeNumber -> {
                    if (char.isDigit()) return State.WholeNumber
                    if (char == '.') return State.IncompleteDecimalNumber
                }
                State.IncompleteDecimalNumber -> {
                    if (char.isDigit()) return State.DecimalNumber
                }
                State.DecimalNumber -> {
                    if (char.isDigit()) return State.DecimalNumber
                }
                State.Add -> {
                    if (char == '-') return State.Subtract
                }
                State.Subtract -> {
                    if (char == '-') return State.Add
                }
            }
            return State.NoNextState
        }
)