package com.d0minikt.lib

class Operator(
        val precedence: Byte,
        val isRightAssociative: Boolean = false
)

val operatorMap: Map<Char, Operator> = mapOf(
        '=' to Operator(0),
        '+' to Operator(1),
        '-' to Operator(1),
        '*' to Operator(2),
        '/' to Operator(2),
        '^' to Operator(3, true)
)