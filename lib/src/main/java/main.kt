import com.d0minikt.lib.lexer.Lexer
import com.d0minikt.lib.tree.BinaryExpressionTreeBuilder
import com.d0minikt.lib.tree.FlattenedTree

fun main(args: Array<String>) {
    val tokens = Lexer("5+x+x^3+12x+5*(1+2+x)").tokenize()
    val tree= BinaryExpressionTreeBuilder().createFromInfix(tokens)
    tree.simplify()
    tree.print()
    println("\n")
    val flattenedTree = FlattenedTree.flatten(tree)
    flattenedTree.print()
}