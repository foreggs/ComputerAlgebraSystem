package com.d0minikt.lib.tree

import com.d0minikt.lib.lexer.Lexer
import com.d0minikt.lib.token.*
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Test

internal class SimplificationTest {
    fun simplify(infix: String): String {
        val tokens = Lexer(infix).tokenize()
        val tree = BinaryExpressionTreeBuilder().createFromInfix(tokens)
        tree.simplify()
        val flatTree = flatten(tree)
        return flatTree.joinToString(" ")
    }

    private fun flatten(node: BinaryExpressionTree): MutableList<Token> {
        val tokens = mutableListOf<Token>()

        if (node.token is OperatorToken) {
            tokens.addAll(flatten(node.left!!))
            tokens.addAll(flatten(node.right!!))
            tokens.add(node.token)
        } else if (node.token is ValueToken) {
            tokens.add(node.token)
        } else
            throw Error("Invalid token ${node.token}")

        return tokens
    }

    @Test fun `a^1 = a`() {
        assertEquals("12", simplify("12^1"))
    }
    @Test fun `a^0=1`() {
        assertEquals("1", simplify("12^0"))
    }
    @Test fun `1^a = 1`() {
        assertEquals("1", simplify("1^12"))
    }
    @Test fun `a+0 = a`() {
        assertEquals("x", simplify("x+0"))
        assertEquals("x", simplify("0+x"))
        assertEquals("x", simplify("0+x+0+0"))
        assertEquals("-x", simplify("-0-x"))
    }
    @Test fun `1*a = a`() {
        assertEquals("x", simplify("1*x"))
        assertEquals("5 x *", simplify("5*(1x)"))
        assertEquals("x", simplify("x/1"))
    }
    @Test fun `a*0 = 0`() {
        assertEquals("0", simplify("55 * 0"))
        assertEquals("0", simplify("0 * x"))
        assertEquals("0", simplify("x(0+2)(0)"))
    }
    @Test fun `n^n`() {
        assertEquals("25", simplify("5^2"))
        assertEquals("25", simplify("(-5)^2"))
        assertEquals("25 1/3 ^", simplify("5^(2/3)"))
        assertEquals("-125", simplify("-5^3"))
        assertEquals("0", simplify("0^4"))
        assertEquals("1", simplify("0^0"))
        assertEquals("n -1/3 ^", simplify("n^(-1/3)"))
        assertEquals("0.2 1/3 ^", simplify("5^(-1/3)"))
    }
    @Test fun `n+n = add(n, n)`() {
        assertEquals("25", simplify("10+15"))
        assertEquals("-5", simplify("10-15"))
        assertEquals("-115", simplify("10+-5^3"))
        assertEquals("0", simplify("0+0"))
        assertEquals("1", simplify("0+0+1"))
    }
    @Test fun `n*n = multiply(n, n)`() {
        assertEquals("25", simplify("5*5"))
        assertEquals("10", simplify("100*0.1"))
        assertEquals("0.00001", simplify("0.0001*0.1"))
    }
    @Test fun `n|n = divide(n, n)`() {
        assertEquals("1", simplify("5/5"))
        assertEquals("1000", simplify("100/0.1"))
        assertEquals("0.001", simplify("0.0001/0.1"))
        assertEquals("1/3", simplify("2/6"))
        assertEquals("7", simplify("49/7"))
    }
}