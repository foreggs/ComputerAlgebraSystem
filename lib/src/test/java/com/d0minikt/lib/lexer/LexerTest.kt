package com.d0minikt.lib.lexer

import com.d0minikt.lib.operatorMap
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

internal class LexerTest {
    private fun tokenize(str: String): String = Lexer(str).tokenize().joinToString(" ")

    @Test fun supportsUnsignedWholeNumbers() {
        assertEquals("0", tokenize("0"))
        assertEquals("55", tokenize("55"))
        assertEquals("1235476859012", tokenize("1235476859012"))
    }
    @Test fun supportsUnsignedDecimals() {
        assertEquals("3.141592", tokenize("3.141592"))
        assertThrows<Exception> { tokenize("0.0.1") }
        assertThrows<Exception> { tokenize(".1") }
    }
    @Test fun supportsOperators() {
        for(op in operatorMap.keys) {
            assertEquals("1 $op 2", tokenize("1${op}2"))
        }
    }
    @Test fun ignoresWhitespace() {
        assertEquals("15 + 5", tokenize("15 + 5"))
        assertEquals("1 + 2", tokenize("    1  +  2   "))
        assertEquals("", tokenize(""))
        assertEquals("", tokenize("     "))
    }

    @Test fun simplifiesTrailingOperators() {
        assertEquals("1", tokenize("+1"))
        assertEquals("1", tokenize("--1"))
        assertEquals("-1", tokenize("-1"))
        assertEquals("-1", tokenize("-----1"))
        assertEquals("-x", tokenize("-x"))
        assertEquals("1 + 2", tokenize("1--2"))
        assertEquals("1 + 2", tokenize("1+--2"))
        assertEquals("1 - 2", tokenize("1---2"))
        assertEquals("1 - 2 + 3", tokenize("1---2+3"))
        assertEquals("-1 * 2", tokenize("-1*2"))
        assertEquals("1 + x", tokenize("1--x"))
        assertEquals("( -x )", tokenize("(-x)"))
        assertEquals("( x )", tokenize("(--x)"))
        assertEquals("( x ) / 2", tokenize("(--x)/2"))
    }

    @Test fun supportsVariableMultiplicationAndPower() {
        assertEquals("5 * x", tokenize("5 * x"))
        assertEquals("x ^ 2", tokenize("x2"))
        assertEquals("x * x", tokenize("xx"))
        assertEquals("2 * x ^ 2", tokenize("2x2"))
        assertEquals("0.12 * x", tokenize("0.12x"))
    }

    @Test fun supportsBracketMultiplication() {
        assertEquals("( x ) * ( y )", tokenize("(x)(y)"))
        assertEquals("x * ( y )", tokenize("x(y)"))
        assertEquals("5 * ( y )", tokenize("5(y)"))
        assertEquals("x * y * ( y )", tokenize("xy(y)"))
    }
}