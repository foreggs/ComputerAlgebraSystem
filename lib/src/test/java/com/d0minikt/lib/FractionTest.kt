package com.d0minikt.lib

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows

internal class FractionTest {
    private fun String.f(): Fraction {
        val split: List<Long> = this.split("/").map { it.toLong() }
        return Fraction(split[0], split[1])
    }

    @Test fun comparesFractions() {
        assertEquals(0, "5/4".f().compareTo("5/4".f()))
        assertEquals(0, "5/4".f().compareTo("10/8".f()))
        assert("5/4".f() == "10/8".f())
        assert("12/1".f() == "144/12".f())

        assert("5/4".f() > "4/4".f())
        assert("5/4".f() > "1/1".f())
        assert("12/1".f() > "143/12".f())
        assert("12/1".f() <= "144/12".f())
    }

    @Test fun doesntAllowDenominatorOf0() {
        assertThrows<Exception> {Fraction(1,0)}
        assertDoesNotThrow {Fraction(1,1)}
    }

    @Test fun convertsToStringBasedOnSimplification() {
        assertEquals("1/2", "1/2".f().toString())
        assertEquals("1/2", "42/84".f().toString())
        assertEquals("1", "1/1".f().toString())
        assertEquals("391/419", "391/419".f().toString())
        assertEquals("8", "64/8".f().toString())
        assertEquals("0", "0/8".f().toString())
    }

    @Test fun simplifiesNegatives() {
        assertEquals("1/2".f(), "-1/-2".f())
        assertEquals("-1/2".f(), "-1/2".f())
        assertEquals("-1/2".f(), "1/-2".f())
    }

    @Test fun supportsReciprocal() {
        assertEquals("1/2".f(), Fraction(2).reciprocal())
        assertEquals("2/1".f(), "1/2".f().reciprocal())
    }
    @Test fun supportsNegation() {
        assertEquals("-1/2".f(), "1/2".f().negate())
        assertEquals("1/2".f(), "-1/2".f().negate())
    }

    @Test fun supportsOperators() {
        // +
        assertEquals("1/2".f(), "1/4".f() + "1/4".f())
        assertEquals("3/8".f(), "1/8".f() + "1/4".f())
        // -
        assertEquals(Fraction(0), "1/4".f() - "1/4".f())
        assertEquals("-1/8".f(), "1/8".f() - "1/4".f())
        // *
        assertEquals("3/5".f(), "2/5".f() * "6/4".f())
        assertEquals(Fraction(0), "2/5".f() * Fraction(0))
        // /
        assertEquals("4/15".f(), "2/5".f() / "6/4".f())
        assertThrows<Exception> { "2/5".f() / Fraction(0) }

        // ^
        assertEquals("4/25".f(), "2/5".f().pow(2))
        assertEquals("16/1".f(), "2/1".f().pow(4))
    }

    @Test fun convertsFromBigDecimal() {
        assertEquals("1/50".f(), Fraction.fromBigDecimal(0.02.toBigDecimal()))
        assertEquals("5/1".f(), Fraction.fromBigDecimal(5.toBigDecimal()))
        assertEquals("0/1".f(), Fraction.fromBigDecimal(0.toBigDecimal()))
        assertEquals("100/1".f(), Fraction.fromBigDecimal(100.toBigDecimal()))
    }
}